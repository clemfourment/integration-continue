# Ci/Cd


**`#build`**

Dans un premier temps on fait intallation les objet que l'on a besoin 
Puis on fait le build en compilant le code 

**`#linter`**

Puis ensuis te on lance linter ici on utilise CodeSniffer.
CodeSniffer est un outil de linting pour le code PHP. 
Il permet de vérifier que le code respecte certaines normes et conventions de codage, comme PSR-2.
Il utilise des "standards" qui sont des ensembles de règles qui définissent ce qui est considéré comme un code correct. 
CodeSniffer fournit un certain nombre de standards intégrés (comme PSR-2) et il est également possible de créer ses propres standards.

**`#test`**

Apres sa ce lance phpUnit qui va tester les test unitaire.
Puis va gener un fichier xml qui va etre traite par junit 
et qui donner un compte rendue de c'est test.

**`#Code_Quality`**

Enfin on le job de qualite de code avec codeClimate qui donnera la qualite de code et generra un fichier Json.
